### Odoo-Argentina ### Localización para Argentina repo: pronexo.com


Un Repositorio de la localización argentina.

<img alt="pronexo.com" src="http://fotos.subefotos.com/7107261ae57571ec94f0f2d7363aa358o.png" />
**pronexo.com** - www.pronexo.com


#### Localizaciones Modelos

- **[Brazil](https://github.com/openerpbrasil/l10n_br_core)** - fuente de conceptos avanzados sobre impuestos
- [Colombia](https://github.com/odoo-colombia/odoo-colombia) - De quien hemos tomado todo este contenido e ideas para organizarnos. Gracias!
- [Italia](http://bazaar.launchpad.net/~openobject-italia-core-devs/openobject-italia/italian-addons-7.0/files)
- [España](http://bazaar.launchpad.net/~openerp-spain-team/openerp-spain/7.0/files)
- [Argentina](http://bazaar.launchpad.net/~openerp-l10n-ar-localization/openerp-l10n-ar-localization/trunk/files)
- [Venezuela](https://github.com/odoo-venezuela/odoo-venezuela)
- [Mexico](http://bazaar.launchpad.net/~openerp-mexico-maintainer/openerp-mexico-localization/trunk/files)



#### Relevantes recursos addicionales, non-code
Github esta reservado al codigo, los documentos de soporte o de trabajo residen en otros lugares.

- [Modelo ER para un motor de impuestos flexible](https://www.draw.io/?#G0B9cHHbqndJcAcjRPdzZUWVdNc3M)
- [Mapeo de Conceptos de Retenciones](https://docs.google.com/spreadsheets/d/1XmY7gPa6mYnhTDgcjXyu3Mr8oS9i_7fuSGhKaL2k7Gc/edit?usp=drive_web)
- [Attributos de cuentas en construccion](https://docs.google.com/spreadsheets/d/1rLx4h8SkxwPeNirj8sr_MU2IBp39X4v2s-vbFCYE3l8/edit)
- [Desarollos actuales en el CORE sobre Impuestos](https://github.com/odoo/odoo/pull/219)
- [Problema: Precision decimal de gravamenes como el CREE](https://github.com/odoo/odoo/pull/178)
- [Problema: Variables que faltan para determinar la base de Retenciones](https://github.com/odoo/odoo/issues/187)
- [Retencion compleja: ET383 y ET384](https://docs.google.com/document/d/1AqFQevCE_hXoTyljRY7E3BMVrDPAmNzXVBBvKVA6Ojc/edit#)
- [Introduccion a los cambios de V8 (Anthony Lesuisse) - muy interesante](https://www.youtube.com/watch?v=0GUxV85DDm4&feature=share&t=1h58m20s)
- [Introduccion para desarollo de modulos](http://www.youtube.com/watch?v=0GUxV85DDm4&feature=share&t=5h47m38s)


#### Recursos adicionales optionales
- *[agrégalo](https://github.com/odoo-colombia/odoo-colombia/edit/master/README.md)* que piensas que falta!